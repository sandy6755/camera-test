import { Component, OnInit } from '@angular/core';
import { CameraPreviewOptions, CameraPreviewPictureOptions, CameraPreview } from '@ionic-native/camera-preview/ngx';


@Component({
  selector: 'app-camerahack',
  templateUrl: './camerahack.component.html',
  styleUrls: ['./camerahack.component.scss'],
})
export class CamerahackComponent implements OnInit {
  picture: string;

  cameraOpts = {
    x: 0,
  y: 0,
  width: window.screen.width,
  height: window.screen.height,
  camera: this.cameraPreview.CAMERA_DIRECTION.FRONT,
  toBack: true,
  tapPhoto: true,
  tapFocus: false,
  previewDrag: false,
  storeToFile: false,
  disableExifHeaderStripping: false
  };

  cameraPictureOpts: CameraPreviewPictureOptions = {
    width: 500,
    height: 500,
    quality: 85
  };

  constructor(private cameraPreview: CameraPreview) {}

  ionViewDidLoad() {
 //   this.startCamera();
  }

  async startCamera() {
    this.picture = null;
    const result = await this.cameraPreview.startCamera(this.cameraOpts);
    console.log(result);
  }

  switchCamera() {
    this.cameraPreview.switchCamera();
  }

  async takePicture() {
    const result = await this.cameraPreview.takePicture(this.cameraPictureOpts);
    await this.cameraPreview.stopCamera();
    this.picture = `data:image/jpeg;base64,${result}`;
  }
  ngOnInit(){
  
  }
}
