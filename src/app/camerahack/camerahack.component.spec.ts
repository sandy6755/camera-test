import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CamerahackComponent } from './camerahack.component';

describe('CamerahackComponent', () => {
  let component: CamerahackComponent;
  let fixture: ComponentFixture<CamerahackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CamerahackComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CamerahackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
