import { Component, OnInit, ɵConsole } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CameraPreview, CameraPreviewOptions,
  CameraPreviewPictureOptions } from '@ionic-native/camera-preview/ngx';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public folder: string;
  imgurl:string;
  private cameraOpts:CameraPreviewOptions = {
    x: 0,
    y: 0,
    width: window.innerWidth,
    height: window.innerHeight, 
    toBack: true,
   
  };
  cameraPictureOpts: CameraPreviewPictureOptions = {
    width: 320,
    height:320,
    quality: 100
  };

  constructor(private activatedRoute: ActivatedRoute, 
    private cameraPreview: CameraPreview) {
  
   }
   ionViewDidLoad() {
    this.taskphoto();
  }
  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id');
  }
 
  async taskphoto(){
    this.imgurl = ''
    const result=  await this.cameraPreview.startCamera(this.cameraOpts)
    console.log(result)
  }
  switchCamera() {
    this.cameraPreview.switchCamera();
  }

  async takePicture() {
    const result = await this.cameraPreview.takePicture(this.cameraPictureOpts);
    await this.cameraPreview.stopCamera();
    console.log(result);
    this.imgurl = `data:image/jpeg;base64,${result}`;

  }
  stop(){
    this.cameraPreview.stopCamera()
  }
  
}
